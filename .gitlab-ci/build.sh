#!/bin/bash

set -e

if [ -n "${DEBUG}" ]; then
  set -x
fi

export tag="${CI_COMMIT_TAG:-${CUSTOM_TAG:-v0.0.0+1}}"
export version="${tag%%+*}"
export patchlevel="${tag##*+}"
tmpdir="$(mktemp -d)"
if [ ! -d "${tmpdir}/${URL##*//}" ]; then
    git clone --depth 1 -b "${version}" "${URL}" "${tmpdir}/${URL##*//}" ||
        git clone --depth 1 "${URL}" "${tmpdir}/${URL##*//}";
fi
cd "${tmpdir}/${URL##*//}"
export GOOS=linux GOARCH="${GOARCH}" GOARM="${GOARM}" CGO_ENABLED=1
export GOFLAGS="-tags=osusergo,netgo,static"
case "${GOARCH}" in
    "arm64")
        dpkg --add-architecture arm64
        ;;
esac
apt-get -qq update
apt-get -qqy install protobuf-compiler
case "${GOARCH}" in
    "arm64")
        apt-get -qqy install libc6:arm64 gcc-aarch64-linux-gnu
        export CC=aarch64-linux-gnu-gcc
        ldd_grep="not a dynamic executable"
        ;;
esac
sed -i 's,^\(INTERNALLDFLAGS :=.*$\),\1 -s -w -buildmode=pie -linkmode=external -extldflags=-static-pie,' Makefile
make build
for name in nextcloud-spreed-signaling nextcloud-spreed-signaling-proxy
do
  mkdir -p "${tmpdir}/package_root/etc/signaling"
  mkdir -p "${tmpdir}/package_root/usr/bin"
  mkdir -p "${tmpdir}/package_root/usr/share/signaling"
  case "${name}" in
    "nextcloud-spreed-signaling")
      name="nextcloud-spreed-signaling"
      artifact="signaling"
      unit="signaling"
      config="server.conf"
      description="Standalone signaling server for Nextcloud Talk"
      ;;
    "nextcloud-spreed-signaling-proxy")
      name="nextcloud-spreed-signaling-proxy"
      artifact="proxy"
      unit="signaling-proxy"
      config="proxy.conf"
      description="Standalone signaling server for Nextcloud Talk - Proxy"
      ;;
  esac
  ldd bin/"${artifact}" |& grep -q "${ldd_grep:-statically linked}"
  cp -v "bin/${artifact}" "${tmpdir}/package_root/usr/bin/${unit}"
  cp -v "${config}.in" "${tmpdir}/package_root/usr/share/signaling/${config}"
  apt-get -qqy install ruby-dev ruby-ffi
  gem install specific_install
  gem specific_install https://github.com/root360/fpm.git all_fixes
  fpm \
    --input-type dir \
    --output-type deb \
    --force \
    --name "${name}" \
    --package "${CI_PROJECT_DIR}/${name}_${version//[a-zA-Z]/}+${patchlevel}_${GOARCH}${GOARM}.deb" \
    --architecture "${GOARCH}${GOARM}" \
    --version "${version//[a-zA-Z]/}+${patchlevel}" \
    --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --url "${URL}" \
    --description "${description}" \
    --prefix "/" \
    --depends systemd \
    --deb-recommends morph027-keyring \
    --deb-systemd "${CI_PROJECT_DIR}/.packaging/${unit}.service" \
    --deb-systemd-enable \
    --deb-systemd-auto-start \
    --deb-activate-noawait /etc/init.d \
    --after-install "${CI_PROJECT_DIR}/.packaging/after-install.sh" \
    --chdir "${tmpdir}/package_root" \
    etc usr
done
