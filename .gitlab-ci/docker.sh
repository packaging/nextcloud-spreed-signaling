#!/bin/bash

set -e

export tag="${CI_COMMIT_TAG:-v0.0.0+1}"
export version="${tag%%+*}"
docker login -u "${CI_REGISTRY_USER}" -p "${CI_JOB_TOKEN}" "${CI_REGISTRY}"
docker buildx create --name multiarch
docker buildx use multiarch
docker buildx inspect --bootstrap

if [ -n "$CI_COMMIT_TAG" ]; then
  buildx="docker buildx build --push"
else
  buildx="docker buildx build"
fi
${buildx} --platform ${PLATFORMS} \
  --build-arg version="${version}" \
  --cache-from "$CI_REGISTRY/$(echo "$CI_PROJECT_NAMESPACE" | tr '[:upper:]' '[:lower:]')/$CI_PROJECT_NAME:latest" \
  -t "$CI_REGISTRY/$(echo "$CI_PROJECT_NAMESPACE" | tr '[:upper:]' '[:lower:]')/$CI_PROJECT_NAME:${CI_COMMIT_REF_SLUG}" \
  -t "$CI_REGISTRY/$(echo "$CI_PROJECT_NAMESPACE" | tr '[:upper:]' '[:lower:]')/$CI_PROJECT_NAME:latest" \
  -f .docker/Dockerfile .
