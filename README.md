[[_TOC_]]

# [Standalone signaling server for Nextcloud Talk](https://github.com/strukturag/nextcloud-spreed-signaling) Packages

>
> **IMPORTANT - Versioning migration**
>
> As of 2020-09-07, the upstream repo includes version numbers/releases. As this repo started with version numbers based on timestamp, the new versions will always be lower than the old versions.
>
> If you already installed one of the old versions, just remove it, update the repo and re-install the package (should then pull something like v0.2.0-1)
>
> `sudo apt-get update && sudo apt-get -y remove nextcloud-spreed-signaling=2020* && sudo apt-get -y install nextcloud-spreed-signaling`
>

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).

## Architectures

* amd64
* arm64 (e.g. Raspberry Pi 4)

## APT

Requirements:

* systemd (Ubuntu >= 16.04)
* all requirements listed [upstream](https://github.com/strukturag/nextcloud-spreed-signaling)
  * Janus
  * Nats ([Installation](https://docs.nats.io/nats-server/installation), [Configuration](https://docs.nats.io/nats-server/configuration), [APT packages](https://gitlab.com/packaging/nats-server))
  * ...

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-nextcloud-spreed-signaling.asc https://packaging.gitlab.io/nextcloud-spreed-signaling/gpg.key
```

### Add repo to apt

```bash
echo "deb [arch=amd64] https://packaging.gitlab.io/nextcloud-spreed-signaling signaling main" | sudo tee /etc/apt/sources.list.d/morph027-nextcloud-spreed-signaling.list
```

### Install packages

#### Signaling server

```bash
sudo apt-get update
sudo apt-get install -y nextcloud-spreed-signaling morph027-keyring
```

#### Signaling proxy server

```bash
sudo apt-get update
sudo apt-get install -y nextcloud-spreed-signaling-proxy morph027-keyring
```

### Configuration

#### Signaling server

```bash
cp /usr/share/signaling/server.conf /etc/signaling/server.conf
```

#### Signaling proxy server

```bash
cp /usr/share/signaling/proxy.conf /etc/signaling/proxy.conf
```

## Docker

### Run

Image builds for amd64 and arm64 (e.g. Raspberry Pi 4) can be found [here](https://gitlab.com/packaging/nextcloud-spreed-signaling/container_registry).

Examples:

```bash
# bind mount override with default listener
docker run -d --name signaling -v /my-local-folder/server.conf:/config/server.conf:ro -p 8081:8081 registry.gitlab.com/packaging/nextcloud-spreed-signaling:latest

# bind mount with proper setting and config with listener on 18081
docker run -d --name signaling -v /my-local-folder/server.conf:/my-config/server.conf:ro -e CONFIG=/my-config/server.conf -e 18081:18081 registry.gitlab.com/packaging/nextcloud-spreed-signaling:latest
```

**NOTICE**: When running in Docker and you're also running Janus, CoTURN, Nats, ... in docker, you need to make sure the containers can reach each other, e.g. creating a dedicated network: `docker network create hbp`.
You can then attach containers using `--network hbp` (or much better: using docker-compose) and use the container names in the config files.

## Extras

### unattended-upgrades

To enable automatic upgrades using `unattended-upgrades`, just add the following config file:

```bash
cat > /etc/apt/apt.conf.d/50nextcloud-spreed-signaling <<EOF
Unattended-Upgrade::Allowed-Origins {"morph027:signaling";};
EOF
```
