FROM golang:1.23 AS builder

ARG version

RUN apt-get -qq update \
 && apt-get -qqy install protobuf-compiler \
 && git clone -b "${version}" https://github.com/strukturag/nextcloud-spreed-signaling \
 && cd nextcloud-spreed-signaling \
 && make build

FROM alpine:3.21

ENV CONFIG=/config/server.conf

RUN adduser -D spreedbackend \
 && apk add --no-cache \
  ca-certificates \
  libc6-compat \
  libstdc++

USER spreedbackend

COPY --from=builder /go/nextcloud-spreed-signaling/bin/signaling /usr/local/bin/signaling

COPY --from=builder /go/nextcloud-spreed-signaling/server.conf.in /config/server.conf

COPY --from=builder /go/nextcloud-spreed-signaling/bin/proxy /usr/local/bin/proxy

COPY --from=builder /go/nextcloud-spreed-signaling/proxy.conf.in /config/proxy.conf

CMD ["/bin/sh", "-c", "exec /usr/local/bin/signaling --config=$CONFIG"]
