# Dev

## build

```bash
DOCKER_DEFAULT_PLATFORM=linux/amd64 docker run --rm -it -v $PWD:/build --tmpfs /tmp:exec -w /tmp/ -e URL="https://github.com/strukturag/nextcloud-spreed-signaling" -e CI_COMMIT_TAG=v1.2.0+2 -e GOARCH=arm64 -e CI_PROJECT_DIR=/build golang:1.23
```
